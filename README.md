# Cognito Driver
> A test driver Angular app for @ngauth/cognito.

_Work in progress_


This is a test/demo app for 
the [@ngauth/cognito](https://gitlab.com/ngauth/cognito) library,
which comprises a number of auth-related reusable components and services.


The library is available as an Angular module on NPM:    
[@ngauth/cognito](https://www.npmjs.com/package/@ngauth/cognito)


A live _work-in-progress_ demo app is available here:    
[NgAuth Cognito Demo](https://ngauth.gitlab.io/cognito-driver)


_It should be noted that the library is currently in _alpha_ state,_
_and this demo app works only to the extent that the underlying functionalities are available in the library._
_Furthermore, this demo app is the frontend only (not hooked up with AWS),_
_and hence it will never be fully functional._


The primary purpose of this demo project
is to illustrate the use of `@ngauth/cognito` APIs (components and services).

