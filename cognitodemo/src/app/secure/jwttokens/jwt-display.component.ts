import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;

import { LoggedInCallback, CommonCallback } from '../../core/auth-callbacks';
import { TokenPair } from '../../common/token-pair';
import { TokenPairContainer, AccessTokenCallback, IdTokenCallback } from '../common/token-callbacks';
import { DefaultAuthRoutes } from '../../route/default-auth-routes';
import { CognitoCommonUtil } from '../../services/cognito/cognito-common-util';
import { UserLoginHelper } from '../../services/cognito/user-login-helper';


@Component({
  selector: 'ngauth-modal-jwt-display',
  templateUrl: './jwt-display.component.html'
})
export class JwtDisplayComponent implements OnInit, TokenPairContainer, LoggedInCallback {

  public tokenPair: TokenPair = new TokenPair();

  constructor(
    private router: Router,
    private authRoutes: DefaultAuthRoutes,
    private cognitoUtil: CognitoCommonUtil,
    private userService: UserLoginHelper
  ) {
  }

  ngOnInit(): void {
    this.userService.isAuthenticated(this);
    if(isDL()) dl.log("in JwtDisplayComponent");
  }

  isLoggedIn(message: string, isLoggedIn: boolean) {
    if (!isLoggedIn) {
      // tbd:
      this.router.navigate([this.authRoutes.authRouteInfo.login]);
      // ...
    } else {
      this.cognitoUtil.getAccessToken(new AccessTokenCallback(this));
      this.cognitoUtil.getIdToken(new IdTokenCallback(this));
    }
  }
}
