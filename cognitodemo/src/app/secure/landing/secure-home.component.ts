import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;

import { LoggedInCallback } from '../../core/auth-callbacks';
import { DefaultAuthRoutes } from '../../route/default-auth-routes';
import { UserLoginHelper } from '../../services/cognito/user-login-helper';


// Secured page demo
// ...

@Component({
  selector: 'ngauth-modal-secure-home',
  templateUrl: './secure-home.component.html'
})
export class SecureHomeComponent implements OnInit, LoggedInCallback {

  constructor(
    private router: Router,
    private authRoutes: DefaultAuthRoutes,
    private userService: UserLoginHelper
  ) {
  }

  ngOnInit() {
    this.userService.isAuthenticated(this);
    if(isDL()) dl.log("SecureHomeComponent: constructor");
  }

  isLoggedIn(message: string, isLoggedIn: boolean) {
    if (!isLoggedIn) {
      // tbd:
      this.router.navigate([this.authRoutes.authRouteInfo.login]);
      // ...
    }
  }
}

