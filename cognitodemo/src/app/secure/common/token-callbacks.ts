import { CommonCallback } from '../../core/auth-callbacks';
import { TokenPair } from '../../common/token-pair';


export interface TokenPairContainer {
  readonly tokenPair: TokenPair;
}

export class AccessTokenCallback implements CommonCallback {
  constructor(public tpc: TokenPairContainer) {
  }

  callback() {
  }

  callbackWithParam(result) {
    this.tpc.tokenPair.accessToken = result;
  }
}

export class IdTokenCallback implements CommonCallback {
  constructor(public tpc: TokenPairContainer) {
  }

  callback() {
  }

  callbackWithParam(result) {
    this.tpc.tokenPair.idToken = result;
  }
}
