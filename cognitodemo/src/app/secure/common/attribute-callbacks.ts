import { CommonCallback } from '../../core/auth-callbacks';
import { NameValuePair } from '../../common/name-value-pair';
// import { CognitoCommonUtil } from '../../services/cognito-common-util';


export interface UserAttributeContainer {
  readonly attributes: Array<NameValuePair>;
}

export class UserAttributeCallback implements CommonCallback {
  
    constructor(
      private uac: UserAttributeContainer, 
      private cognitoIdentity: string) {
    }
  
    callback() {
    }
  
    callbackWithParam(result: any) {
      for (let i = 0; i < result.length; i++) {
        let parameter = new NameValuePair();
        parameter.name = result[i].getName();
        parameter.value = result[i].getValue();
        this.uac.attributes.push(parameter);
      }
      let param = new NameValuePair()
      param.name = "cognito ID";
      param.value = this.cognitoIdentity;
      this.uac.attributes.push(param)
    }
  }
  