import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;

import { LoggedInCallback, CommonCallback } from '../../core/auth-callbacks';
import { NameValuePair } from '../../common/name-value-pair';
import { UserAttributeContainer, UserAttributeCallback } from '../common/attribute-callbacks';
import { DefaultAuthRoutes } from '../../route/default-auth-routes';
import { CognitoCommonUtil } from '../../services/cognito/cognito-common-util';
import { UserLoginHelper } from '../../services/cognito/user-login-helper';
import { UserAttributesHelper } from '../../services/cognito/user-attributes-helper';


@Component({
  selector: 'ngauth-modal-user-profile',
  templateUrl: './user-profile.component.html'
})
export class UserProfileComponent implements OnInit, UserAttributeContainer, LoggedInCallback {

  public attributes: Array<NameValuePair> = [];
  public cognitoId: String;

  constructor(
    private router: Router,
    private authRoutes: DefaultAuthRoutes,
    private cognitoUtil: CognitoCommonUtil,
    private userService: UserLoginHelper, 
    private userAttrs: UserAttributesHelper
  ) {
  }

  ngOnInit(): void {
    this.userService.isAuthenticated(this);
    if(isDL()) dl.log("In UserProfileComponent");
  }

  isLoggedIn(message: string, isLoggedIn: boolean) {
    if (!isLoggedIn) {
      // tbd:
      this.router.navigate([this.authRoutes.authRouteInfo.login]);
      // ...
    } else {
      this.userAttrs.getAttributes(new UserAttributeCallback(this, this.cognitoUtil.getCognitoIdentity()));
    }
  }
}
