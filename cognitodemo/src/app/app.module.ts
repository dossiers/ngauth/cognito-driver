import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { NgCoreCoreModule } from '@ngcore/core';
import { NgAuthCoreModule } from '@ngauth/core';
import { NgAuthServicesModule } from '@ngauth/services';
import { UserAuthServiceFactory } from '@ngauth/services';
import { NgAuthCognitoModule } from '@ngauth/cognito';
import { CognitoUserAuthServiceFactory } from '@ngauth/cognito';
import { NgAuthFormsModule } from '@ngauth/forms';
import { NgAuthModalsModule } from '@ngauth/modals';

import { MaterialComponentsModule } from './material-components.module';

import { AppConfig } from '@ngcore/core';
import { CustomConfig, ConfigFactory } from '@ngcore/core';
import { DefaultAuthRoutes } from '@ngauth/core';
import { DefaultCognitoAuthConf } from '@ngauth/core';
import { AuthConfigManager } from '@ngauth/services';
import { CognitoCommonUtil, DdbServiceUtil, AwsServiceHelper } from '@ngauth/cognito';
import { UserRegistrationHelper, UserLoginHelper, UserAttributesHelper } from '@ngauth/cognito';

import { DemoAuthRoutes } from './route/demo-auth-routes';
import { DemoCognitoAuthConf } from './config/demo-cognito-auth-conf';

// import {
//   UserLoginModalComponent,
//   UserLogoutModalComponent,
//   NewRegistrationModalComponent,
//   ResendCodeModalComponent,
//   ConfirmRegistrationModalComponent,
//   ChangePasswordModalComponent,
//   ResetPasswordModalComponent
// } from '@ngauth/modals';

// import { SecureHomeComponent } from './secure/landing/secure-home.component';
// import { JwtDisplayComponent } from './secure/jwttokens/jwt-display.component';
// import { UserActivityComponent } from './secure/activity/user-activity.component';
// import { UserProfileComponent } from './secure/profile/user-profile.component';

import { AppComponent } from './app.component';


@NgModule({
  declarations: [
    // SecureHomeComponent,
    // JwtDisplayComponent,
    // UserActivityComponent,
    // UserProfileComponent,
    AppComponent,
    // UserLoginModalComponent,
    // UserLogoutModalComponent,
    // NewRegistrationModalComponent,
    // ResendCodeModalComponent,
    // ConfirmRegistrationModalComponent,
    // ChangePasswordModalComponent,
    // ResetPasswordModalComponent
  ],
  imports: [
    BrowserModule,
    FormsModule, ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    RouterModule.forRoot([{ path: '', component: AppComponent }]),
    NgCoreCoreModule.forRoot(),
    NgAuthCoreModule.forRoot(),
    NgAuthServicesModule.forRoot(),
    NgAuthCognitoModule.forRoot(),
    NgAuthFormsModule.forRoot(),
    NgAuthModalsModule.forRoot(),
    MaterialComponentsModule,
  ],
  providers: [
    // AppConfig,
    { provide: APP_INITIALIZER, useFactory: (config: AppConfig) => () => { config.load(); }, deps: [AppConfig], multi: true },
    // ConfigFactory,
    DemoCognitoAuthConf,
    // AuthConfigManager,
    // // { provide: APP_INITIALIZER, useFactory: (config: AppConfig, authConfigManager: AuthConfigManager) => () => { config.load(); authConfigManager.triggerLoading(); }, deps: [AppConfig, DemoCognitoAuthConf, ConfigFactory, AuthConfigManager], multi: true },
    // // { provide: APP_INITIALIZER, useFactory: (config: AppConfig, authConfigManager: AuthConfigManager) => () => { config.load(); authConfigManager.loadConfig().subscribe((authConfig)=> {console.log(">>> authConfig loaded.");})}, deps: [AppConfig, ConfigFactory, AuthConfigManager], multi: true },
    // DefaultAuthRoutes,
    { provide: DefaultAuthRoutes, useClass: DemoAuthRoutes },
    // DefaultCognitoAuthConf,
    { provide: DefaultCognitoAuthConf, useClass: DemoCognitoAuthConf },
    // CognitoCommonUtil,
    // DdbServiceUtil,
    // CognitoCommonUtil,
    // DdbServiceUtil,
    // AwsServiceHelper,
    // UserRegistrationHelper,
    // UserLoginHelper,
    // UserAttributesHelper,
    // UserAuthServiceFactory,
    { provide: UserAuthServiceFactory, useClass: CognitoUserAuthServiceFactory }
  ],
  entryComponents: [
    // AppComponent,
    // UserLoginModalComponent,
    // UserLogoutModalComponent,
    // NewRegistrationModalComponent,
    // ResendCodeModalComponent,
    // ConfirmRegistrationModalComponent,
    // ChangePasswordModalComponent,
    // ResetPasswordModalComponent,
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {
  constructor(
    private authConfigManager: AuthConfigManager
  ) {
    // Normally, it's best to call it here (or, in APP_INITIALIZER).
    // But, for testing, we deliberately start the appwith authconfig not loaded...
    // ...
    // this.authConfigManager.triggerLoading();
    // ...
  }
}
